import numpy as np
import matplotlib.pyplot as plt
data = np.genfromtxt('/store/uzh/uzh8/ESC401/exercise1/data.txt',delimiter = ',')
fig,ax = plt.subplots(figsize = (8,2))
ax.scatter(data[:,0],data[:,1])
plt.savefig('figure_data.png',dpi=350)
