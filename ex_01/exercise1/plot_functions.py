import numpy as np
import matplotlib.pyplot as plt

fig,ax = plt.subplots()

x = np.linspace(-np.pi,np.pi,200)
ax.plot(x,np.sin(x),label = 'sin(x)')
ax.plot(x,np.cos(x),label = 'cos(x)')
ax.plot(x,-x/5,label = '-x/5')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.legend()
plt.savefig('figure_ex3.png',dpi = 350)
