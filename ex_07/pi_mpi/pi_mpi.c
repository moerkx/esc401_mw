#include <stdio.h>
#include "mpi.h"

int main(int argc, char *argv[])
{
	MPI_Init(NULL, NULL);

	int size, rank;
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	int N = 2*1000*1000*1000;
	int chunk_size = N/size;
	int start = chunk_size*rank;
	if (rank == 0)
	{
		start = 1;
	}
	int stop = chunk_size*(rank+1);
	if (rank == size-1)
	{
		stop = N;
	}	

	double partial_sum = 0.0, total_sum = 0.0;
	for (double k=start; k<stop; k=k+4)
	{
		partial_sum += 1.0/k;
		partial_sum -= 1.0/(k+2.0);
	}

	MPI_Reduce(&partial_sum, &total_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

	if (rank == 0)
	{
		double pi_approx = 4*total_sum; 
		printf("Pi is approximately %f\n", pi_approx);
	}
	MPI_Finalize();
	return 0;
}
