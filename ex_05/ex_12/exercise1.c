#include <stdio.h>
#include <stdlib.h>


typedef struct complex_vector
{
	int real;
	int imag;
} cv;

int linear_combination(cv* x, cv* y, cv* z, float a, int length)
{
	for (int k=0; k<length; k++)
	{
		z[k].real = x[k].real + y[k].real * a;
		z[k].imag = x[k].imag + y[k].imag * a;
	}
	return 0;
}

void print_cv(cv* z, int length)
{
	printf("z = {\n");
	for (int k=0; k<length; k++)
	{
		printf("{%d, %di", z[k].real, z[k].imag);
	}
	printf("}\n\n");
}

int main ()
{
	int N = 1000;
	float a = 2;

	cv* x;
	cv* y;
	cv* z;
	x = malloc(sizeof(cv)*N);
	y = malloc(sizeof(x));
	z = malloc(sizeof(x));


	linear_combination(x, y, z, a, N);
	print_cv(z, N);	
	return 0;
}
