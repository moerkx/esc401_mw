import re
import os
import matplotlib.pyplot as plt

def main():
    x = [100, 500, 1000, 5000, 10000, 20000, 50000]
    y01 = {}
    y12 = {}

    for file in os.listdir():
        if "times" in file:
            with open(file) as fil:
                for line in fil.readlines():
                    if "real" in line:
                        try:
                            m_index = line.index("m")
                            time = float(line[m_index+1:-2])

                            if "12_" in file:
                                y12[int(file[8:-4])] = time
                            else:
                                y01[int(file[5:-4])] = time
                        except:
                            pass
    print(y01)
    y01 = dict(sorted(y01.items()))
    y12 = dict(sorted(y12.items()))
    print(y01)

    y01 = [y01[k] for k in sorted(y01.keys())]
    y12 = [y12[k] for k in sorted(y12.keys())]


    fig, axis = plt.subplots()
    axis.plot(x, y01, "b", label="1 thread")
    axis.plot(x, y12, "r", label="12 threads")

    axis.set_xlabel("N particles")
    axis.set_ylabel("real time [s]")
    axis.legend()

    plt.savefig("new_time_plot")
    plt.show()

if __name__ == "__main__":
    main()
